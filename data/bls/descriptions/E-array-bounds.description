## Match: regexp:[^:]*:[0-9]\+\(:[0-9]\+\)\?: warning:\/ array subscript is above array bounds
## Extract: simple 0 2 ':'
## Since: 6
The build log contains the term
<pre>
warning: array subscript is above array bounds
</pre>
<p>
This means the compiler thinks an array is accessed beyond its maximum length,
most likely meaning some buffer overflow.
</p>
<p>Also note that since C99, the old hack of having an array of length 1 was replaced
with another method and that accessing a fixed length array after its end is undefined
behaviour, so the compiler is free to assume it never happens when optimising.
(i.e. accessing any but the first element of an array of length 1 is an error and no false positive).
</p>
<p>In the unlikely case you find any false positives please let me know, so I can list here possible
cases or downgrade this tag to a warning.</p>
