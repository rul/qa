#!/usr/bin/perl

# Copyright (C) 2014-2020 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use warnings;
use DB_File;
use DBI;
use JSON;

my $db_dir = "/srv/qa.debian.org/data/vcswatch";
my $db_filename = "$db_dir/vcswatch-new.db";

my $dbh = DBI->connect("dbi:Pg:service=qa", 'qa', '',
	{AutoCommit => 1, RaiseError => 1, PrintError => 0});
$dbh->do("SET client_encoding = 'utf-8'");

# write .db and .json output
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
	or die "Can't open database $db_filename : $!";
my @arr;

my $q = $dbh->prepare("SELECT * FROM vcs WHERE last_scan IS NOT NULL");
$q->execute;
while (my $pkg = $q->fetchrow_hashref) {
	push @arr, $pkg;
	my $package = $pkg->{package};
	$db{"$package:vcs"} = $pkg->{vcs} if ($pkg->{vcs});
	$db{"$package:browser"} = $pkg->{browser} if ($pkg->{browser});
	$db{"$package:status"} = $pkg->{status};
	if ($pkg->{status} eq 'COMMITS') {
		$db{"$package:version"} = $pkg->{tag} . '+' . $pkg->{commits};
		$db{"$package:version"} =~ s!^debian/!!; # shorten tag name
	} else {
		$db{"$package:version"} = $pkg->{changelog_version};
	}
	if ($pkg->{error}) {
		$pkg->{error} =~ s/"/'/g; # poor man's quoting fix
		$db{"$package:error"} = $pkg->{error};
	}
	if ($pkg->{edited_by}) {
		$pkg->{edited_by} =~ s/"/'/g; # poor man's quoting fix
		$db{"$package:error"} .= "Vcs info edited by $pkg->{edited_by}";
	}
	$db{"$package:avatar"} = $pkg->{avatar} if ($pkg->{avatar});
	$db{"$package:issues"} = $pkg->{issues} if ($pkg->{issues});
	$db{"$package:merge_requests"} = $pkg->{merge_requests} if ($pkg->{merge_requests});
	$db{"$package:ci_status"} = $pkg->{ci_status} if ($pkg->{ci_status});
	$db{"$package:ci_url"} = $pkg->{ci_url} if ($pkg->{ci_url});
}

open JSON, "| gzip > $db_dir/vcswatch-new.json.gz";
print JSON encode_json(\@arr);
close JSON;
